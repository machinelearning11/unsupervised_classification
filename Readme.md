## K Nearest Neighbours
======================

- Used for where Distribution Pattern is Unspecified
- Lazy Learning Algorithm/Example Based Learning like Find-S Algorithm

#### PROS

- Easy to train. Needs only the dataset
- Works with multiple classes
- Parameters Required: k and Distance Metric

#### CONS

- High Prediction Cost
- Not good with high dimensional data
- Categorical Features don't work well

## ABOUT THE CODE
================

#### KNN Implementation

- **3 classes**: CSV Handler, K Nearest neighours Algorithm, PerformanceMetrics Calculation
- **Input Required**: File location and No. of neighbours
- **Performance Metrics**: Accuracy, Recall, Precision and Confusion Matrix
- Handles only **numeric** Data
- Name of Data File Present: **KNN_Project_Data**, **Classified_Data**
- **Possible Errors**: No Scaling has been done yet

## K-Means Clustering

It is an unsupervised learning method on unsupervised data. The process involves grouping
data based on a specified number of groups

**CENTROID**: centroids are the centres of clusters used to label a new data
Each centroid is a collection of feature values which define the resulting groups.

Folowing are the attributes for KMeansCluster class:

- Centroid cluster_centers[];

- Point data_points[];

- Point class has variables x, cluster_id and y

- Centroid class has variables x,y and cluster_id

**HOW TO USE**

```java
class Main {
	public static void main(String args[]) {
        KMeansCluster kmeans = new KMeansCluster(n); // n = number of centers
        kmeans.fit(dataset, x, y); 
        // x = index of column datavalues for x-axis
        // y = index of column datavalues for y-axis
	}
}
```
#### OUTPUT FOR KMEANS

```
Enter absolute path for Dataset:
KNN_Project_Data

Enter number of clusters
10

Enter the feature name for x values:
[XVPM, GWYH, TRAT, TLLZ, IGGA, HYKR, EDFS, GUUB, MGJM, JHZC]
GWYH
Enter the feature name for y values
IGGA
Starting Clustering Process...
Number of clusters = 10
Cluster Centers: 

CLuster Instance:0
x:0.447
y:0.364

CLuster Instance:1
x:0.333
y:0.59

CLuster Instance:2
x:0.431
y:0.506

CLuster Instance:3
x:0.544
y:0.302

CLuster Instance:4
x:0.569
y:0.481

CLuster Instance:5
x:0.365
y:0.494

CLuster Instance:6
x:0.384
y:0.697

CLuster Instance:7
x:0.519
y:0.578

CLuster Instance:8
x:0.483
y:0.781

CLuster Instance:9
x:0.311
y:0.427


OUTPUT SHOWING CLUSTER FAMILY FOR EACH DATA POINT:

0,1,2,0,4,5,6,7,1,1,8,6,1,7,1.....
```