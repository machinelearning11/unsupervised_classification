package com.ipsit;

class PerformanceMetrics {
	double[][] confusion_matrix = new double[2][2];
	double accuracy, precision, recall;
	private int[] y_predict;
	private int[] y_test;

	PerformanceMetrics(int[] y_pred, int[] y_t) {
		y_predict = y_pred;
		y_test = y_t;
		calculate_metrics();
	}

	private void calculate_metrics() {
		int len = y_predict.length;
		double tp = 0, tn = 0, fp = 0, fn = 0;
		for (int i = 0; i < len; i++) {
			if (y_predict[i] == y_test[i]) {
				if (y_predict[i] == 0) {
					tn++;
				} else {
					tp++;
				}
			} else {
				if (y_predict[i] == 1) {
					fp++;
				} else {
					fn++;
				}
			}
		}
		confusion_matrix[0][0] = tp;
		confusion_matrix[0][1] = fp;
		confusion_matrix[1][0] = fn;
		confusion_matrix[1][1] = tn;
		accuracy = (tp + tn) / len;
		precision = tp / (tp + fp);
		recall = tp / (tp + fn);
	}
}
