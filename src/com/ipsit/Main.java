package com.ipsit;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		// write your code here
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter absolute path for Dataset:");
		String file_path = sc.nextLine();

		//Main.executeKNN(sc, file_path);
		Main.executeKMeans(sc, file_path);
	}

	private static void executeKMeans(Scanner sc, String file_path) throws IOException {
		CSVHandler csvHandler = new CSVHandler(file_path, false);
		csvHandler.create_data();
		System.out.println("Enter number of clusters");
		int n = sc.nextInt();
		KMeansCluster kmeans = new KMeansCluster(n);
		sc.nextLine();
		System.out.println("Enter the feature name for x values");
		System.out.println(Arrays.toString(csvHandler.feature_names));
		String x = sc.nextLine();
		System.out.println("Enter the feature name for y values");
		String y = sc.nextLine();
		kmeans.fit(csvHandler, x, y);
		System.out.println("Number of clusters = "+ kmeans.numberOfClusters);
		System.out.println("Cluster Centers: ");
		for (int i = 0; i < kmeans.cluster_centers.length; i++) {
			System.out.println("CLuster Instance:" + kmeans.cluster_centers[i].cluster_id);
			System.out.println("x:" + kmeans.cluster_centers[i].x);
			System.out.println("y:" + kmeans.cluster_centers[i].y);
			System.out.println("Final Status:" + kmeans.cluster_centers[i].no_change);
		}
		for (int i = 0; i < kmeans.data_points.length; i++) {
			System.out.print(kmeans.data_points[i].cluster_id + ",");
		}
		System.out.println();
	}

	private static void executeKNN(Scanner sc, String file_path) throws IOException {
		CSVHandler csvHandler = new CSVHandler(file_path, false);
		csvHandler.create_data();
		System.out.println("Enter number of neighbours");
		int n = sc.nextInt();
		KNeighbours knn = new KNeighbours(n);
		System.out.println(Arrays.toString(csvHandler.feature_names));
		int[] y_pred = knn.predict(csvHandler.input_set, csvHandler.input_set, csvHandler.output);
		// System.out.println(Arrays.toString(csvHandler.output));
		// System.out.println(Arrays.toString(y_pred));
		PerformanceMetrics perf_metrics = new PerformanceMetrics(y_pred, csvHandler.output);
		System.out.printf("Accuracy: %f\n", perf_metrics.accuracy);
		System.out.printf("Recall: %f\n", perf_metrics.recall);
		System.out.printf("Precision: %f\n", perf_metrics.precision);
		System.out.printf("Confusion Matrix:\n%.2f\t%.2f\n%.2f\t%.2f\n",
				perf_metrics.confusion_matrix[0][0], perf_metrics.confusion_matrix[0][1],
				perf_metrics.confusion_matrix[1][0], perf_metrics.confusion_matrix[1][1]);
	}
}
