package com.ipsit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

// Focusing on getting double Data Only
class CSVHandler {
	// Private Access Variables
	private String filename;
	private ArrayList<double[]> input_set_list = new ArrayList<>();
	private ArrayList<Integer> output_list = new ArrayList<>();
	private ArrayList<String> column_headers_list = new ArrayList<>();

	// Default Access Variables
	double input_set[][];
	int output[];
	private boolean col_header;
	String[] feature_names, column_headers;

	CSVHandler(String filen, boolean col) {
		filename = filen;
		col_header = col;
	}

	private void parseline(String s, boolean header) {
		String[] sarr = s.split(",");
		int len = sarr.length;
		if (header) {
			feature_names = Arrays.copyOfRange(sarr, 0, len - 1);
		} else {
			if (col_header) {
				column_headers_list.add(sarr[0]);
				input_set_list.add(Arrays.stream(Arrays.copyOfRange(sarr, 1, len - 1)).mapToDouble(Double::parseDouble).toArray());
			} else {
				input_set_list.add(Arrays.stream(Arrays.copyOfRange(sarr, 0, len - 1)).mapToDouble(Double::parseDouble).toArray());
			}
			output_list.add(Integer.parseInt(sarr[len - 1]));
		}
	}

	void create_data() throws IOException {
		File file = new File(filename);
		String s;
		if (file.exists()) {
			Scanner sc = new Scanner(file);
			parseline(sc.nextLine(), true);
			while (sc.hasNextLine()) {
				s = sc.nextLine();
				parseline(s, false);
			}
		}
		int training_set_rows = input_set_list.size();
		int training_set_cols = input_set_list.get(0).length;
		input_set = new double[training_set_rows][training_set_cols];
		output = new int[training_set_rows];
		for (int i = 0; i < training_set_rows; i++) {
			input_set[i] = input_set_list.get(i);
			output[i] = output_list.get(i);
			if(col_header) {
				column_headers[i] = column_headers_list.get(i);
			}
		}
	}
}
