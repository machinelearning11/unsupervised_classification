package com.ipsit;

import java.util.*;

class KMeansCluster {
	int numberOfClusters;
	private int number_of_points;
	// double[][] cluster_centers;
	Centroid cluster_centers[];
	Point data_points[];

	KMeansCluster(int n) {
		this.numberOfClusters = n;
	}

	private double[][] get_data_points(double[][] input_set, int index_x, int index_y) {
		double[][] values = new double[2][input_set.length];
		// MaxMinScaling of data
		double x_max = Double.MIN_VALUE, x_min = Double.MAX_VALUE, y_max = Double.MIN_VALUE, y_min = Double.MAX_VALUE;
		for (int i = 0; i < input_set.length; i++) {
			values[0][i] = input_set[i][index_x];
			if (values[0][i] > x_max) {
				x_max = values[0][i];
			}
			if (values[0][i] <= x_min) {
				x_min = values[0][i];
			}
			values[1][i] = input_set[i][index_y];
			if (values[1][i] > y_max) {
				y_max = values[1][i];
			}
			if (values[1][i] <= y_min) {
				y_min = values[1][i];
			}
		}
		for (int i = 0; i < input_set.length; i++) {
			values[0][i] = (values[0][i] - x_min)/(x_max - x_min);
			values[1][i] = (values[1][i] - y_min)/(y_max - y_min);
		}
		return values;
	}

	void fit(CSVHandler csvHandler, String x, String y) {
		int index_x = Arrays.asList(csvHandler.feature_names).indexOf(x);
		int index_y = Arrays.asList(csvHandler.feature_names).indexOf(y);
		double[][] axis_values;
		if (index_x != -1 || index_y != -1) {
			axis_values = get_data_points(csvHandler.input_set, index_x, index_y);
		} else {
			return;
		}
		/*
		 * 0 index in axis_values indicates x_axis values
		 * 1 index in axis_values indicates y_axis values
		 */
		this.number_of_points = axis_values[0].length;
		this.cluster_centers = new Centroid[this.numberOfClusters];
		this.data_points = new Point[number_of_points];

		/*
		 * Data Formation: Associating points to a Point Object Array
		 * Cluster Centers to a Point object Array
		 */

		for (int i = 0; i < this.numberOfClusters; i++) {
			this.cluster_centers[i] = new Centroid();
			this.cluster_centers[i].x = Math.round(axis_values[0][i] * 1000.0) / 1000.0;
			this.cluster_centers[i].y = Math.round(axis_values[1][i] * 1000.0) / 1000.0;
			this.cluster_centers[i].cluster_id = i;
			this.cluster_centers[i].no_change = false;
		}
		for (int i = 0; i < number_of_points; i++) {
			this.data_points[i] = new Point();
			this.data_points[i].x = axis_values[0][i];
			this.data_points[i].y = axis_values[1][i];
		}
		start_clustering();
	}

	private void start_clustering() {
		/*
		 * Start the cluster allocation
		 */
		System.out.println("Starting Clustering Process...");
		for (int i = 0; i < this.number_of_points; i++) {
			this.data_points[i] = search_closest_points(this.data_points[i], this.cluster_centers);
		}
		/*
		 * Next Step: Iterative Process for recreating cluster centers
		 * and checking if there are no changes then stop the process
		*/
		boolean no_more_chnages = false;
		while (!no_more_chnages) {
			for (int i = 0; i < this.number_of_points; i++) {
				this.data_points[i] = search_closest_points(this.data_points[i], this.cluster_centers);
			}
			no_more_chnages = recreate_cluster_centers();
		}

	}

	private Point search_closest_points(Point p, Centroid[] centroids) {
		double min_dist = Double.MAX_VALUE, dist;
		int correct_cluster = 0;
		for (int i = 0; i < this.numberOfClusters; i++) {
			dist = Math.sqrt(Math.pow((p.x - centroids[i].x), 2) - Math.pow((p.y - centroids[i].y), 2));
			if (dist < min_dist) {
				min_dist = dist;
				correct_cluster = centroids[i].cluster_id;
			}
		}
		p.cluster_id = correct_cluster;
		return p;
	}

	private boolean recreate_cluster_centers() {

		boolean centroid_has_changed = false;
		for (int i = 0; i < this.numberOfClusters; i++) {
			final int check = i;
			Object[] points_obj = Arrays.stream(this.data_points).filter(p -> p.cluster_id == check).toArray();
			Point[] req_points = new Point[points_obj.length];
			for (int j = 0; j < req_points.length; j++) {
				req_points[j] = new Point();
			}
			for (int j = 0; j < points_obj.length; j++) {
				req_points[j] = (Point) points_obj[j];
			}

			double x_sum = 0, y_sum = 0, number_of_req_points = req_points.length;
			for (Point req_point : req_points) {
				x_sum += req_point.x;
				y_sum += req_point.y;
			}
			Centroid new_centroid = new Centroid();
			new_centroid.x = Math.round((x_sum / number_of_req_points) * 1000.0) / 1000.0;
			new_centroid.y = Math.round((y_sum / number_of_req_points) * 1000.0) / 1000.0;
			if (new_centroid.x != cluster_centers[i].x || new_centroid.y != cluster_centers[i].y) {
				cluster_centers[i].x = new_centroid.x;
				cluster_centers[i].y = new_centroid.y;
				cluster_centers[i].no_change = false;
			} else {
				cluster_centers[i].no_change = true;
			}
		}
		for (int i = 0; i < this.numberOfClusters; i++) {
			centroid_has_changed = !cluster_centers[i].no_change;
		}
		return centroid_has_changed;
	}
}

class Point {
	double x, y;
	int cluster_id;
}

class Centroid {
	double x, y;
	int cluster_id;
	boolean no_change;
}