package com.ipsit;

import java.util.*;
import java.util.stream.Collectors;

class KNeighbours {
	private int no_of_neighbours;

	KNeighbours(int n) {
		no_of_neighbours = n;
	}

	private double calculate_euclidean_distance(double[] data, double[] curr_data) {
		int len = data.length;
		double dist = 0;
		for (int i = 0; i < len; i++) {
			dist += Math.pow(data[i] - curr_data[i], 2);
		}
		return Math.sqrt(dist);
	}


	private boolean calculate_neighbours(double[][] dataset, double[] current_data, int[] target_class) {
		int i, j, rows = dataset.length, cols = dataset[0].length;
		HashMap<Integer, Double> distance = new HashMap<>();
		for (i = 0; i < rows; i++) {
			distance.put(i, calculate_euclidean_distance(dataset[i], current_data));
		}
		HashMap<Integer, Double> sorted_distance = distance.entrySet().stream().sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(e1, e2) -> e1, LinkedHashMap::new));
		int class1 = 0, class0 = 0, count = 0;
		for (int key : sorted_distance.keySet()) {
			count++;
			if (target_class[key] == 1) {
				class1++;
			} else {
				class0++;
			}
			if (count == no_of_neighbours) {
				break;
			}
		}
		return class1 > class0;
	}

	int[] predict(double[][] X_train, double[][] X_test, int[] y_train) {
		int[] y_predicted = new int[X_test.length];
		for (int i = 0; i < X_test.length; i++) {
			boolean res = calculate_neighbours(X_train, X_test[i], y_train);
			y_predicted[i] = (res) ? 1 : 0;
		}
		return y_predicted;
	}
}
